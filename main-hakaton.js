var config, specs;


 var libsAndModules= {
    app: "src/app",
    text: "src/text",
    jquery: 'libs/jquery/jquery.min',
    jasmine: 'libs/jasmine/jasmine',
    sockJs: 'libs/websocket/sockjs-0.3.4',
    stomp: 'libs/websocket/stomp',
    bootstrap: 'libs/bootstrap/bootstrap',
    boot: 'libs/jasmine/boot',
    angular: 'libs/angular/angular',
    'angular-route': 'libs/angular-route/angular-route',
    'jasmine-html': 'libs/jasmine/jasmine-html',
    'angular-mocks': 'libs/angular-mocks/angular-mocks',
    'ctrs-module' : 'src/controllers/controllers.module',
    'serv-module' : 'src/services/services.module',
    'directives-module' : 'src/directives/directives.module'

  };




var controllers = {
        appCtr: "src/controllers/appCtr"
};

var services = {
        chatService: "src/services/chat.service"
};

var directives = {
        chatBarDirective: "src/directives/chat-bar.directive"
};

for(var key in directives){
  if(directives.hasOwnProperty(key)){
    libsAndModules[key]=directives[key];
  }
}


for(var key in controllers){
  if(controllers.hasOwnProperty(key)){
    libsAndModules[key]=controllers[key];
  }
}


for(var key in services){
  if(services.hasOwnProperty(key)){
    libsAndModules[key]=services[key];
  }
}


config = {
  baseUrl: '.',
  paths : libsAndModules,
  shim: {
   jquery: {
      exports: '$'
    },
   bootstrap: {
      deps: ['jquery'],
      exports: 'bootstrap'
    },
   angular: {
      deps: ['bootstrap'],
      exports: 'angular'
    },
    'angular-mocks' : {
        deps: ['angular'],
        exports: 'angular-mocks'
    },
    'angular-route': {
        deps: ['angular'],
        exports: 'angular-route'
    },
    app: {
        deps: ['angular', 'angular-route', 'sockJs', 'stomp' ],
        exports: 'app'
    },
    jasmine: {
      exports: 'jasmine'
    },
    'jasmine-html': {
      deps: ['jasmine']
    },
    exports: 'jasmine',
    boot: {
      deps: ['jasmine', 'jasmine-html'],
      exports: 'jasmine'
    }
  }
};



require.config(config);


require(["angular", 'app'], function (angular, app){
    "use strict";

    angular.element(document).ready(function() {
        angular.bootstrap(document, [app.name]);
    });
});





  


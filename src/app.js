define([ "angular",
	"text!./src/views/layout.html", 
	'ctrs-module', 
	'serv-module',
    'directives-module',
	'appCtr', 
	'chatService', 
	'chatBarDirective',
    'chatPopupDirective'
     ],
     function(angular, layoutTmpl){
    	var app = angular.module('onegit', [ "ngRoute", 'controllers', 'services', 'directives']); 
    
    	app.config(['$routeProvider', function($routeProvider){

       		 $routeProvider.when('/onegit', {
            	        template: layoutTmpl,
               		controller: 'appCtr'
        	});
    	}]);

		 app.config(function($httpProvider) {
			 //Enable cross domain calls
			 $httpProvider.defaults.useXDomain = true;
			 $httpProvider.defaults.headers.common['Access-Control-Allow-Origin'] = '*';
			 //Remove the header used to identify ajax call  that would prevent CORS from working
			 delete $httpProvider.defaults.headers.common['X-Requested-With'];
		 });

		 return app;
});

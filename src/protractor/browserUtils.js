/**
 * Created by midima on 4/16/2016.
 */
var BrowserUtils = function() {
    this.baseUrl = 'http://localhost:8000/#/login';

    this.waitForURL = function(url_expected)
    {
        return browser.driver.wait(function() {
                return browser.driver.getCurrentUrl().then(function(url) {
                    return url.indexOf(url_expected) !== -1;
                });
            }, 60000, 'It\'s taking too long to load ' + url_expected + '!'
        );
    };

    this.waitForElementToLoad = function(locator)
    {
        browser.driver.wait(function() {
            return browser.isElementPresent(by.id(locator));
        }, 30000);

        return browser.wait(function () {
            return element(by.id(locator)).isDisplayed();
        }, 30000, element + 'should be loaded');
    };

    this.waitForCSSElementToLoad = function(locator)
    {
        browser.driver.wait(function() {
            return browser.isElementPresent(by.css(locator));
        }, 30000);

        return browser.wait(function () {
            return element(by.css(locator)).isDisplayed();
        }, 30000, element + 'should be loaded');
    };

    this.waitAttributeLocator = function(attribute, locator, attributeVal, notContains)
    {
        return browser.wait(function () {
            return locator.getAttribute(attribute).then(function (attr) {
                if(notContains)
                    return !(attr.indexOf(attributeVal) !== -1);
                else
                    return (attr.indexOf(attributeVal) !== -1);
            });
        }, 10000);
    };

    this.waitForTextToBeDisplayed = function(messageElem)
    {
        return browser.wait(function() {
            return messageElem.getText().then(function (text) {
                return (text !== '');
            });
        }, 30000, 'message should not be empty');
    };

};

module.exports = new BrowserUtils();

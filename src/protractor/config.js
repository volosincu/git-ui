/**
 * Created by midima on 4/16/2016.
 */
var Jasmine2HtmlReporter = require ('protractor-jasmine2-html-reporter');

exports.config = {

    // The address of a running selenium server.
    seleniumAddress: 'http://localhost:4444/wd/hub',

    chromeOnly: false,

    capabilities: {
        'browserName': 'chrome'
    },

    onPrepare: function() {
        browser.driver.manage().window().maximize();

        jasmine.getEnv().addReporter(
            new Jasmine2HtmlReporter({
                savePath: './src/protractor/reports',
                takeScreenshots: false
            })
        );
    },

    allScriptsTimeout: 60000,
    getPageTimeout: 60000,

    baseUrl: 'http://localhost:8000/#/login',
    rootElement: 'html',

    suites: {
        main: ['tests/test1.js'],
        branchesList: ['tests/branches_tests.js'],
        chat: ['tests/chat_tests.js']
    },

    // Options to be passed to Jasmine-node.
    jasmineNodeOpts: {
        showColors: true, // Use colors in the command line report.
        defaultTimeoutInterval: 600000
    }
};
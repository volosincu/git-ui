/**
 * Created by midima on 4/16/2016.
 */
describe('Branches list tests', function () {
    var utils = require('../browserUtils.js');
    var branchesList = require('../pages/branches_list.js');

    beforeEach(function () {
        browser.driver.get(utils.baseUrl);
        browser.ignoreSynchronization = true;
        browser.waitForAngular();
    });

    it('check that chat functionality is available per branches', function () {
        branchesList.checkViewIsDisplayed();
        console.log('done');
    });

});

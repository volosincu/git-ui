/**
 * Created by midima on 4/16/2016.
 */
describe('Chat tests', function () {
    var utils = require('../browserUtils.js');
    var chatBox = require('../pages/chat.js')

    beforeEach(function () {
        browser.driver.get(utils.baseUrl);
        browser.ignoreSynchronization = true;
        browser.waitForAngular();
    });

    it('check that chat functionality is available per branches', function () {
        chatBox.checkViewIsDisplayed();
    });

});


/**
 * Created by midima on 4/16/2016.
 */
describe('Test', function () {
    var utils = require('../browserUtils.js');
    var branchesList = require('../pages/branches_list.js')

    beforeEach(function () {
        browser.driver.get(utils.baseUrl);
        browser.ignoreSynchronization = true;
        browser.waitForAngular();
    });

    it('check application url ', function () {
        expect(browser.getCurrentUrl()).toContain('#/login');
        branchesList.checkViewIsDisplayed();
    });

});



/**
 * Created by midima on 4/16/2016.
 */
var BranchesList = function() {
    var utils = require('../browserUtils.js');

    var view_ID = 'view';

    this.view = element(by.id(view_ID));

    this.checkViewIsDisplayed = function(){
        utils.waitForElementToLoad(view_ID);
        expect(this.view.isDisplayed()).toBe(true);
    };

};

module.exports = new BranchesList();
define(['angular' ], function(angular){

	var module = angular.module('services',[]);


	var ChatService = module.factory('ChatService', ['$http', '$q', function($http, $q){
	
		return {
			postMs : function(){
				console.log('post message');	
			},
			getChatConversations: function(branchId) {
				console.log("chat conversation");
				var deffered = $q.defer();
				var promise = $http.get("http://localhost:8080/api/chat/conversations/" + branchId);
				promise.success(function (data) {
					console.log("[success]", data);
					deffered.resolve(data);
				}).error(function(error){
					console.log("[error]", error);
					deffered.reject(error);
				});

				return deffered.promise;
			}
		}
	}]);

	return ChatService;
	


});

define(['angular'], function (angular) {
    var stompClient = null;
    var baseUrl = "http://localhost:8080";

    angular.module('directives').directive('chatPopup', function () {
        return {
            scope: {},
            controller: ['$scope', 'ChatService', function ($scope, ChatService) {

                    $scope.initChatConversation = function(branchId) {
                        ChatService.getChatConversations(branchId).then(function(data){
                            console.log("[data]", data);
                            $scope.data = data;
                        }, function(err){
                            console.log("[err]", err);
                        });
                    };
                    //get conversations and approved



                    $scope.connect = function () {
                        console.log("connect");
                        $scope.chatId = Math.random() > 0.5 ? "1" : "2";
                        var socket = new SockJS(baseUrl + '/chat');
                        stompClient = Stomp.over(socket);
                        stompClient.connect({}, function (frame) {
                            //setConnected(true);
                            console.log('Connected at: ' + $scope.chatId);
                            stompClient.subscribe('/topic/channel/' + $scope.chatId, function (chatRow) {
                                console.log("chatRow", chatRow);
                                $scope.addChatRow(JSON.parse(chatRow.body));
                            });
                        });
                    };

                    $scope.disconnect = function () {
                        console.log("disconnect");
                        if (stompClient != null) {
                            stompClient.disconnect();
                        }
                        //setConnected(false);
                        console.log("Disconnected");
                    };

                    $scope.sendName = function () {
                        console.log("send name");
                        var name = document.getElementById('name').value;
                        var chatRow = {
                            'userId': 'alice',
                            branchId: $scope.chatId,
                            date: new Date(),
                            content: name,
                            isApproved: false
                        };

                        stompClient.send("/app/chat", {branchId: $scope.chatId}, JSON.stringify(chatRow));
                    };

                    $scope.addChatRow = function (chatRow) {
                        console.log("add chat row");
                        if (chatRow.branchId != $scope.chatId) {
                            console.log("yeeeeeeeeeeee");
                            return;
                        }
                        console.log("show greetings", chatRow);
                        var response = document.getElementById('response');
                        var p = document.createElement('p');
                        p.style.wordWrap = 'break-word';
                        p.appendChild(document.createTextNode(chatRow.date));
                        response.appendChild(p);

                        //TODO handle this to add one new row in chat
                    };
                    $scope.clickButton = function () {
                        console.log("click button");
                    }
                }
            ],
            templateUrl: 'src/directives/chat-popup.directive.html',
            link: function (scope, elem, attrs) {
                console.log("directive.com");
                scope.initChatConversation("123");
            }
        };
    });

});


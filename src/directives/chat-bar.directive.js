define(['angular'], function (angular) {
    var stompClient = null;
    var baseUrl = "http://localhost:8080";

    angular.module('directives').directive('chatBar', function () {
        return {
            scope: {},
            controller: ['$scope',
                function ($scope) {
                    console.log("controller");
                }
            ],
            templateUrl: 'src/directives/chat-bar.directive.html',
            link: function (scope, elem, attrs) {
                console.log("directive.com");
            }
        };
    });

});



*Prerequisites 

    npm - nodejs package manager (comes with the installation of nodejs)
    grunt - javascript task runner npm install -g grunt-cli
    bower - web package manager npm install -g bower

	
To build the project independently the following steps are required : 

	
	 install the following grunt plugins

    npm install grunt --save-dev
    npm install grunt-contrib-copy --save-dev
    npm install grunt-contrib-uglify --save-dev
    npm install grunt-sync --save-dev

    get the dependencies with bower - jasmine, require
	
    run:  bower install

    - pentru dev profile ruleaza 'python -m http.server' in root folder
    - ruleaza comanda 'grunt hakaton' pentru build demo
    - in profilul hakaton fisierul main.js este suprascris de versiunea main-hakaton.js



    http://localhost:8000/#/login





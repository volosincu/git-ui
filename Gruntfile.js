/**
 * Created by bogdan.volosincu on 15/04/2016.
 */
module.exports = function(grunt) {




    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        
        copy: {

	    dev : {
                files: [
                    {expand: true, src: ['./main-dev.js'], dest: 'build/theapp/src/main'},
	        ]
	    },

	    hakaton : {
                files: [
                    {expand: false, src: ['./main-hakaton.js'], dest: 'build/theapp/src/main.js', flatten : true},
	        ]
	    },

            source: {
                files: [
                    {expand: true, src: ['./*.html'], dest: 'build/theapp'},
		    {expand: true, src: ['./style/*.css'], dest: 'build/theapp'},
                    {expand: true, src: ['./src/*.js'], dest: 'build/theapp'},
                    {expand: true, src: ['./src/*/*.js', './src/*/*.html'], dest: 'build/theapp'}
                ]
            },
            libs: {
                files: [
                    {expand: true, src: ['./libs/websocket/*.js'], dest: 'build/theapp/libs/websocket/', flatten: true},
                    {expand: true, src: ['./libs/requirejs/*.js', './libs/underscore/*.js'], dest: 'build/theapp'},
                    {expand: true, src: ['./libs/jasmine/lib/jasmine-core/*.js', './libs/jasmine/lib/jasmine-core/*.css'], dest: 'build/theapp/libs/jasmine', flatten: true},
                    {expand: true, src: ['./libs/jquery/dist/jquery.min.js'], dest: 'build/theapp/libs/jquery', flatten: true},
	            {expand: true, src: ['./libs/angular/angular.js'], dest: 'build/theapp/libs/angular', flatten: true},
                    {expand: true, src: ['./libs/angular-route/angular-route.js'], dest: 'build/theapp/libs/angular-route', flatten: true},
                    {expand: true, src: ['./libs/angular-mocks/angular-mocks.js'], dest: 'build/theapp/libs/angular-mocks', flatten: true},
                    {expand: true, src: ['./libs/bootstrap/dist/js/bootstrap.js'], dest: 'build/theapp/libs/bootstrap', flatten: true}
                ]
            },
	    css: {
	   	files :[
                    {expand: true, src: ['./libs/bootstrap/dist/css/bootstrap.css'], dest: 'build/theapp/style/', flatten: true}
		] 


            }
        },


        protractor: {
            options: {
                keepAlive: true,
                configFile: "./src/protractor/config.js"
            },
            run: {}
        }
    });


    grunt.loadNpmTasks('grunt-sync');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-protractor-runner');


    grunt.registerTask('dev', ['copy:source', 'copy:libs', 'copy:dev']);
    grunt.registerTask('hakaton', ['copy:source', 'copy:libs', 'copy:hakaton']);
    grunt.registerTask('e2e', ['protractor:run']);

};
